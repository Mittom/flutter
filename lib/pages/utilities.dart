String floorNum(nbr, cc) {
  if (cc.localeName == "en") {
    if (nbr == 1) return cc.floor(cc.first);
    else if (nbr == 2) return cc.floor(cc.second);
    else if (nbr == 3) return cc.floor(cc.third);
    else return cc.floor(cc.ordinal(nbr));
  }

  return cc.floor(cc.ordinal(nbr));
}

String statusTranslate(String str, cc) {
  switch (str.toLowerCase()) {
    case 'available':
      return cc.available;
    case 'used':
      return cc.used;
    case 'out':
      return cc.out;
    default:
      return cc.unknown;
  }
}

String typeTranslate(String str, cc) {
  switch (str.toLowerCase()) {
    case 'wash':
      return cc.wash;
    case 'dry':
      return cc.dry;
    default:
      return cc.unknown;
  }
}
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_hf/api/model.dart';
import 'package:flutter_hf/bloc/details_bloc.dart';
import 'package:flutter_hf/pages/utilities.dart';

class DetailsPageWidget extends StatefulWidget {
  final int machineID;

  DetailsPageWidget(this.machineID, {Key? key}) : super(key: key);

  @override
  _DetailsPageWidgetState createState() => _DetailsPageWidgetState(machineID);
}

class _DetailsPageWidgetState extends State<DetailsPageWidget>{
  final int machineID;
  late Completer _refreshCompleter;

  _DetailsPageWidgetState(this.machineID);

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer();
  }

  @override
  Widget build(BuildContext context) {
    final l10n = L10n.of(context)!;
    return Scaffold(
        appBar: AppBar(
          title: Text(l10n.mainTitle),
        ),
      body: BlocProvider(
        create: (_) => DetailsBloc()..add(LoadItemEvent(machineID)),
        child: BlocBuilder<DetailsBloc, DetailsState>(
          builder: (context, state) {
            if (state is ItemLoading || state is ItemRefresh) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is ItemData) {
              return RefreshIndicator(
                onRefresh: () async {
                  BlocProvider.of<DetailsBloc>(context).add(RefreshItemEvent(machineID));
                  return _refreshCompleter.future;
                },
                child: BlocListener<DetailsBloc, DetailsState>(
                  listener: (_, state) {
                    if (!(state is ItemRefresh)) {
                      _refreshCompleter.complete();
                      _refreshCompleter = Completer();
                    }
                  },
                  child: ListView(
                    children: [
                      DetailsItem(state.machine)
                    ],
                  )
                )
              );
            } else {
              return Stack(
                children: <Widget>[
                  Center(
                    child: Center(
                      child: Text(l10n.somethingWrong)
                    )
                  ),
                  ListView()
                ],
              );
            }
          }
        ),
      )
    );
  }
}

class DetailsItem extends StatelessWidget {
  final MachineItem item;

  const DetailsItem(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = L10n.of(context)!;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: item.iconImage != null ? CachedNetworkImage(
                imageUrl: item.iconImage!,
                height: 200,
                fit: BoxFit.cover,
              ) : Icon(
              Icons.image,
              size: 200,
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(l10n.type, style: const TextStyle(fontSize: 18)),
                      Text(typeTranslate(item.type, l10n), style: const TextStyle(fontSize: 18)),
                    ],
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(l10n.status, style: const TextStyle(fontSize: 18)),
                      Text(statusTranslate(item.state, l10n), style: const TextStyle(fontSize: 18)),
                    ],
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(l10n.vendor, style: const TextStyle(fontSize: 18)),
                      Text(item.vendor ?? l10n.unknown, style: const TextStyle(fontSize: 18)),
                    ],
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(l10n.location, style: const TextStyle(fontSize: 18)),
                      Text(floorNum(item.floor, l10n), style: const TextStyle(fontSize: 18)),
                    ],
                  )
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
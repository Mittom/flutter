import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_hf/api/model.dart';
import 'package:flutter_hf/bloc/list_bloc.dart';
import 'package:flutter_hf/pages/utilities.dart';

class ListPageWidget extends StatefulWidget {
  @override
  _ListPageWidgetState createState() => _ListPageWidgetState();
}

class _ListPageWidgetState extends State<ListPageWidget> {
  late Completer _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer();
  }

  @override
  Widget build(BuildContext context) {
    final l10n = L10n.of(context)!;
    return BlocProvider(
      create: (_) => ListBloc()..add(LoadListEvent()),
      lazy: true,
      child: BlocListener<ListBloc, ListState>(
        listener: (context, state) {
          if (state is ListError) {
            context.showSnackBar(
              content: Text(l10n.failedRefresh),
            );
          }
        },
        child: BlocBuilder<ListBloc, ListState>(
          builder: (context, state) {
            if (state is ListLoading || state is ListRefresh) {
              return Scaffold(
                appBar: AppBar(
                  title: Text(l10n.mainTitle),
                ),
                body: Center(
                  child: CircularProgressIndicator(),
                )
              );
            } else if (state is ListItem) {

              void filter({hideOutFilter, typeFilter, floorFilter, statusFilter}) async {
                BlocProvider.of<ListBloc>(context).add(RefreshListEvent(
                  hideOutFilter: hideOutFilter ?? state.hideOutFilter,
                  typeFilter: typeFilter ?? state.typeFilter,
                  floorFilter: floorFilter ?? state.floorFilter,
                  statusFilter: statusFilter ?? state.statusFilter,
                ));
              }

              return Scaffold(
                appBar: AppBar(
                  title: Text(l10n.mainTitle),
                  actions: [ListPageHeadWidget(state, filter)],
                ),
                body: RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<ListBloc>(context).add(RefreshListEvent());
                    return _refreshCompleter.future;
                  },
                  child: BlocListener<ListBloc, ListState>(
                    listener: (_, state) {
                      if (!(state is ListRefresh)) {
                        _refreshCompleter.complete();
                        _refreshCompleter = Completer();
                      }
                    },
                    child: state.machines.isEmpty ?
                      Stack(
                       children: <Widget>[
                        Center(
                          child: Center(
                            child: Text(l10n.emptyList),
                          )
                        ),
                        ListView()
                        ]
                      ) :
                      ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: state.machines.length,
                        shrinkWrap: true,
                        itemBuilder: (context, i) {
                          return ContentItem(state.machines[i]);
                        }
                      ),
                  )
                )
              );
            } else {
              return Scaffold(
                appBar: AppBar(
                  title: Text(l10n.mainTitle),
                ),
                body: Stack(
                  children: <Widget>[
                    Center(
                        child: Center(
                          child: Text(l10n.somethingWrong),
                        )
                    ),
                    ListView()
                  ],
                )
              );
            }
          },
        ),
      )
    );
  }
}

class ListPageHeadWidget extends StatelessWidget {
  final state;
  final filter;

  const ListPageHeadWidget(this.state, this.filter, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = L10n.of(context)!;
    return PopupMenuButton(
      itemBuilder: (context) {
        return [
          PopupMenuItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(l10n.hideOut),
                Checkbox(
                  value: state.hideOutFilter ?? false,
                  onChanged: (bool? newValue) {
                    filter(hideOutFilter: newValue);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
          PopupMenuItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(l10n.typeFilter),
                DropdownButton<String>(
                  value: state.typeFilter ?? "All",
                  onChanged: (String? newValue) {
                    filter(typeFilter: newValue);
                    Navigator.pop(context);
                  },
                  items: <String>['All', 'Wash', 'Dry']
                    .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value != "All" ? typeTranslate(value, l10n) : l10n.all),
                      );
                    }).toList(),
                ),
              ],
            ),
          ),
          PopupMenuItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(l10n.floorFilter),
                DropdownButton<String>(
                  value: state.floorFilter ?? "All",
                  onChanged: (String? newValue) {
                    filter(floorFilter: newValue);
                    Navigator.pop(context);
                  },
                  items: <String>["All", '1', '2', '3', '4', '5', '6', '7', '8', '9']
                    .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value != "All" ? floorNum(int.parse(value), l10n) : l10n.all),
                      );
                    }).toList(),
                ),
              ],
            ),
          ),
          PopupMenuItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(l10n.statusFilter),
                DropdownButton<String>(
                  value: state.statusFilter ?? "All",
                  onChanged: (String? newValue) {
                    filter(statusFilter: newValue);
                    Navigator.pop(context);
                  },
                  items: <String>['All', 'Available', 'Used', 'Out']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value != "All" ? statusTranslate(value, l10n) : l10n.all),
                    );
                  }).toList(),
                ),
              ],
            ),
          ),
          PopupMenuItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text(l10n.reset),
                  onPressed: (
                      (state.hideOutFilter == false || state.hideOutFilter == null) &&
                      (state.typeFilter == "All" || state.typeFilter == null) &&
                      (state.floorFilter == "All" || state.hideOutFilter == null) &&
                      (state.statusFilter == "All" || state.statusFilter == null)
                  ) ? null : () {
                    filter(hideOutFilter: false, typeFilter: "All", floorFilter: "All", statusFilter: "All");
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ];
      },
    );
  }
}

class ContentItem extends StatelessWidget {
  final MachineListItem item;

  const ContentItem(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = L10n.of(context)!;
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, "/details", arguments: item.id);
        },
        child: Card(
            child: Container(
                height: 80,
                child: Stack(
                    children: [
                      StatusIcon(item.state),
                      Align(
                          alignment: Alignment.center,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(typeTranslate(item.type, l10n), style: const TextStyle(fontSize: 18)),
                                SizedBox(height: 8),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(floorNum(item.floor, l10n))
                                  ],
                                )
                              ]
                          )
                      )
                    ]
                )
            )
        )
    );
  }
}

class StatusIcon extends StatelessWidget {
  final String status;

  const StatusIcon(this.status, {Key? key}) : super(key: key);

  Icon _statusIcon(status) {
    IconData ico;
    Color color;

    switch (status) {
      case 'available':
        ico = Icons.check_circle_outline;
        color = Colors.lightGreen;
        break;
      case 'used':
        ico = Icons.error_outline;
        color = Colors.blueAccent;
        break;
      case 'out':
        ico = Icons.remove_circle_outline;
        color = Colors.redAccent;
        break;
      default:
        ico = Icons.help_outline;
        color = Colors.orangeAccent;
        break;
    }

    return Icon(ico, color: color, size: 40);
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: _statusIcon(status),
    );
  }
}

extension BuildContextHelpers on BuildContext {
  void showSnackBar({required Widget content}) {
    ScaffoldMessenger.of(this).showSnackBar(SnackBar(content: content));
  }
}
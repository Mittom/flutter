import 'package:json_annotation/json_annotation.dart';

part 'model.g.dart';

class MachineListItem{
  final num id;
  final String type;
  final String state;
  final num floor;

  MachineListItem(this.id, this.type, this.state, this.floor);
}

class MachineItem extends MachineListItem {
  final String? vendor;
  final String? iconImage;

  MachineItem(id, type, state, floor, this.vendor, this.iconImage) : super(id, type, state, floor);
}

@JsonSerializable()
class MachinesListResponse{
  final num count;
  final List<MachinesListInformation> machines;

  MachinesListResponse(this.count, this.machines);

  factory MachinesListResponse.fromJson(Map<String, dynamic> json) => _$MachinesListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MachinesListResponseToJson(this);
}

@JsonSerializable()
class MachinesListInformation extends MachineListItem{
  MachinesListInformation(id, type, state, floor) : super(id, type, state, floor);

  factory MachinesListInformation.fromJson(Map<String, dynamic> json) => _$MachinesListInformationFromJson(json);
  Map<String, dynamic> toJson() => _$MachinesListInformationToJson(this);
}

@JsonSerializable()
class MachineInformation extends MachineListItem{
  final String? vendor;
  @JsonKey(name: 'image') final String iconImage;

  MachineInformation(id, type, state, floor, this.vendor, this.iconImage) : super(id, type, state, floor);

  factory MachineInformation.fromJson(Map<String, dynamic> json) => _$MachineInformationFromJson(json);
  Map<String, dynamic> toJson() => _$MachineInformationToJson(this);
}
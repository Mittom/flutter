import 'package:flutter_hf/api/model.dart';
import 'package:flutter_hf/api/service.dart';

class MachineRepository {
  var machinesService = MachinesService();

  Future<List<MachineListItem>> getMachines({
    hideOutFilter,
    typeFilter,
    floorFilter,
    statusFilter
  }) async {
    var response = await machinesService.getMachinesData();
    List<MachineListItem> list = response.machines.map((data) => MachineListItem(
      data.id,
      data.type,
      data.state,
      data.floor,
    ),
    ).toList();

    if (hideOutFilter != null && hideOutFilter) {
      list.removeWhere((element) => element.state == "out");
    }
    if (typeFilter != null && typeFilter != "All") {
      list.removeWhere((element) => element.type != typeFilter.toLowerCase());
    }
    if (floorFilter != null && floorFilter != "All" && int.parse(floorFilter) is int) {
      list.removeWhere((element) => element.floor != int.parse(floorFilter));
    }
    if (statusFilter != null && statusFilter != "All") {
      list.removeWhere((element) => element.state != statusFilter.toLowerCase());
    }

    return list;
  }

  Future<MachineItem> getMachine(id) async {
    var response = await machinesService.getMachineData(id);
    return MachineItem(
      response.id,
      response.type,
      response.state,
      response.floor,
      response.vendor,
      response.iconImage,
    );
  }
}
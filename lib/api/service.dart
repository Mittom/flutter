import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter_hf/api/model.dart';

const _baseUrl = "flutter.typhoon.ktk.bme.hu";

MachinesListResponse _parseListJson(String message){
  return MachinesListResponse.fromJson(jsonDecode(message));
}

MachineInformation _parseItemJson(String message){
  return MachineInformation.fromJson(jsonDecode(message));
}

class MachinesService {
  Future<MachinesListResponse> getMachinesData() async {
    var response = await http.get(Uri.https(_baseUrl, "api/machines")).timeout(Duration(seconds: 30));
    return await compute(_parseListJson, response.body);
  }

  Future<MachineInformation> getMachineData(id) async {
    var response = await http.get(Uri.https(_baseUrl, "api/machines/$id")).timeout(Duration(seconds: 30));
    return await compute(_parseItemJson, response.body);
  }
}
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MachinesListResponse _$MachinesListResponseFromJson(Map<String, dynamic> json) {
  return MachinesListResponse(
    json['count'] as num,
    (json['machines'] as List<dynamic>)
        .map((e) => MachinesListInformation.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MachinesListResponseToJson(
        MachinesListResponse instance) =>
    <String, dynamic>{
      'count': instance.count,
      'machines': instance.machines,
    };

MachinesListInformation _$MachinesListInformationFromJson(
    Map<String, dynamic> json) {
  return MachinesListInformation(
    json['id'],
    json['type'],
    json['state'],
    json['floor'],
  );
}

Map<String, dynamic> _$MachinesListInformationToJson(
        MachinesListInformation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'state': instance.state,
      'floor': instance.floor,
    };

MachineInformation _$MachineInformationFromJson(Map<String, dynamic> json) {
  return MachineInformation(
    json['id'],
    json['type'],
    json['state'],
    json['floor'],
    json['vendor'] as String?,
    json['image'] as String,
  );
}

Map<String, dynamic> _$MachineInformationToJson(MachineInformation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'state': instance.state,
      'floor': instance.floor,
      'vendor': instance.vendor,
      'image': instance.iconImage,
    };

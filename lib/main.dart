import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_hf/pages/list.dart';
import 'package:flutter_hf/pages/details.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Route? generateRoutes(RouteSettings settings) {
    final name = settings.name ?? "";
    if (name.startsWith("/details")) {
      return MaterialPageRoute(
        builder: (context) {
          return DetailsPageWidget(settings.arguments as int);
        },
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Washing Machines",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ListPageWidget(),
      onGenerateRoute: generateRoutes,
      localizationsDelegates: L10n.localizationsDelegates,
      supportedLocales: L10n.supportedLocales,
    );
  }
}

// test
// Beadás előtt flutter clean
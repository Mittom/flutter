import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hf/api/model.dart';
import 'package:flutter_hf/api/repository.dart';

part 'details_event.dart';
part 'details_state.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  DetailsBloc() : super(ItemLoading());

  @override
  Stream<DetailsState> mapEventToState(
    DetailsEvent event,
  ) async* {
    if (event is LoadItemEvent) {
      yield* _mapLoadItemEventToState(event);
    } else if (event is RefreshItemEvent) {
      yield* _mapRefreshItemEventToState(event);
    }
  }

  Stream<DetailsState> _mapLoadItemEventToState(LoadItemEvent event) async* {
    try {
      // print("Loading item...");
      final machine = await MachineRepository().getMachine(event.id);

      yield ItemData(machine);
    } catch (e) {
      print("Machine with ID ${event.id} was not found!");
      yield ItemError();
    }
  }

  Stream<DetailsState> _mapRefreshItemEventToState(RefreshItemEvent event) async* {
    final currentState = state;
    if (currentState is ItemData) {
      yield ItemRefresh(currentState.machine);

      try {
        // print("Refreshing item...");
        final machine = await MachineRepository().getMachine(event.id);

        yield ItemData(machine);
      } catch (e) {
        print("Machine with ID ${event.id} was not found!");
        yield ItemError();
      }
    }
  }
}

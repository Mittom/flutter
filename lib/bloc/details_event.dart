part of 'details_bloc.dart';

abstract class DetailsEvent {
  const DetailsEvent();
}

class LoadItemEvent extends DetailsEvent {
  final int id;

  LoadItemEvent(this.id);
}

class RefreshItemEvent extends DetailsEvent {
  final int id;

  RefreshItemEvent(this.id);
}
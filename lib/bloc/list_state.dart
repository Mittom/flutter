part of 'list_bloc.dart';

abstract class ListState {
  const ListState();
}

class ListItem extends ListState with EquatableMixin {
  final List<MachineListItem> machines;
  bool? hideOutFilter;
  String? typeFilter;
  String? floorFilter;
  String? statusFilter;

  ListItem(this.machines, {
    this.hideOutFilter,
    this.typeFilter,
    this.floorFilter,
    this.statusFilter
  });

  @override
  List<Object> get props => [machines, {
    hideOutFilter,
    typeFilter,
    floorFilter,
    statusFilter
  }];
}

class ListRefresh extends ListState with EquatableMixin {
  final List<MachineListItem> machines;

  ListRefresh(this.machines);

  @override
  List<Object> get props => [machines];
}

class ListError extends ListState with EquatableMixin {
  final List<MachineListItem> machines;

  ListError(this.machines);

  @override
  List<Object> get props => [machines];
}

class ListLoading extends ListState {
  static final ListLoading _instance = ListLoading._();
  factory ListLoading() => _instance;

  ListLoading._();
}
part of 'list_bloc.dart';

abstract class ListEvent {
  const ListEvent();
}

class LoadListEvent extends ListEvent {
  static final LoadListEvent _instance = LoadListEvent._();
  factory LoadListEvent() => _instance;

  LoadListEvent._();
}

class RefreshListEvent extends ListEvent {
  bool? hideOutFilter;
  String? typeFilter;
  String? floorFilter;
  String? statusFilter;

  RefreshListEvent({
    this.hideOutFilter,
    this.typeFilter,
    this.floorFilter,
    this.statusFilter
  });
}
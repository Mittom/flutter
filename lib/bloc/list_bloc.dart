import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hf/api/model.dart';
import 'package:flutter_hf/api/repository.dart';

part 'list_event.dart';
part 'list_state.dart';


class ListBloc extends Bloc<ListEvent, ListState> {
  ListBloc() : super(ListLoading());

  @override
  Stream<ListState> mapEventToState(
      ListEvent event
  ) async* {
    if (event is LoadListEvent) {
      yield* _mapLoadListEventToState();
    } else if (event is RefreshListEvent) {
      yield* _mapRefreshListEventToState(event);
    }
  }

  Stream<ListState> _mapLoadListEventToState() async* {
    try {
      // print("Loading list...");
      final machines = await MachineRepository().getMachines();

      yield ListItem(machines);
    } on Exception catch (e) {
      print("Loading failed, reason: ${e.toString()}");
      yield ListError([]);
    }
  }

  Stream<ListState> _mapRefreshListEventToState(RefreshListEvent event) async* {
    final currentState = state;
    if (currentState is ListItem) {
      final hideOutFilter = event.hideOutFilter ?? currentState.hideOutFilter;
      final typeFilter = event.typeFilter ?? currentState.typeFilter;
      final floorFilter = event.floorFilter ?? currentState.floorFilter;
      final statusFilter = event.statusFilter ?? currentState.statusFilter;


      yield ListRefresh(currentState.machines);

      try {
        // print("Refreshing list...");
        final machines = await MachineRepository().getMachines(
            hideOutFilter: hideOutFilter,
            typeFilter: typeFilter,
            floorFilter: floorFilter,
            statusFilter: statusFilter
        );

        yield ListItem(machines,
            hideOutFilter: hideOutFilter,
            typeFilter: typeFilter,
            floorFilter: floorFilter,
            statusFilter: statusFilter
        );
      } catch (e) {
        print("Refreshing failed, reason: ${e.toString()}");
        yield ListError(currentState.machines);
      }
    }
  }
}

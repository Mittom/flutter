part of 'details_bloc.dart';

abstract class DetailsState {
  const DetailsState();
}

class ItemData extends DetailsState with EquatableMixin {
  final MachineItem machine;

  ItemData(this.machine);

  @override
  List<Object> get props => [machine];
}

class ItemRefresh extends DetailsState with EquatableMixin {
  final MachineItem machine;

  ItemRefresh(this.machine);

  @override
  List<Object> get props => [machine];
}

class ItemError extends DetailsState  {
  static final ItemError _instance = ItemError._();
  factory ItemError() => _instance;

  ItemError._();
}

class ItemLoading extends DetailsState {
  static final ItemLoading _instance = ItemLoading._();
  factory ItemLoading() => _instance;

  ItemLoading._();
}